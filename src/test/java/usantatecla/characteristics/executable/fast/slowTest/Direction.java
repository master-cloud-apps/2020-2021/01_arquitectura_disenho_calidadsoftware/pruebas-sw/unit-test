package usantatecla.characteristics.executable.fast.slowTest;

public enum Direction {
	VERTICAL,
	HORIZONTAL,
	DIAGONAL,
	INVERSE,
	NON_EXISTENT;
}
